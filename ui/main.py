#!/usr/bin/env python

from tkinter import *
from tkinter import ttk
import numpy as np


# Osnovno okno
root = Tk()
root.title('Kalkulator za Gaussov žarek')
osnovno_okno = ttk.Frame(root)
osnovno_okno.pack(fill=BOTH)
# Widget za zavihke
zavihki = ttk.Notebook(osnovno_okno)
zavihki.pack(fill=BOTH)

# ----------------------------------------------------------------------------------------------------------------------
# Definicija spremenljivk za tekstovna polja
valovna_dolzina = StringVar()
polmer_v_pasu = StringVar()
rayleighova_dolzina = StringVar()
divergencni_kot = StringVar()
polmer_na_R_razdalji = StringVar()

fokusna_razdalja = StringVar()
pas_leca = StringVar()
polmer_v_pasu_fokusiranje = StringVar()
rayleighova_dolzina_fokusiranje = StringVar()
leca_pas = StringVar()
polmer_v_pasu_fokusiranje2 = StringVar()
povecava = StringVar()
rayleighova_dolzina_fokusiranje2 = StringVar()

prejsnji_divergenci_kot = []
prejsnji_polmer_na_R_razdalji = []
prejsnja_rayleighova_razdalja = []

# ----------------------------------------------------------------------------------------------------------------------
# Definicija funkcij za gumbe
def izracun_za_zarek(*args):
    try:
        vrednost_valovne_dolzine = float(valovna_dolzina.get())
        vrednost_polmer_v_pasu = float(polmer_v_pasu.get())

        global zR
        zR = (np.pi * vrednost_polmer_v_pasu**2) / vrednost_valovne_dolzine
        global w0
        w0 = vrednost_polmer_v_pasu

        valovna_dolzina.set(vrednost_valovne_dolzine)
        polmer_v_pasu.set(w0)
        rayleighova_dolzina.set(zR)
        divergencni_kot.set(vrednost_valovne_dolzine / (np.pi * vrednost_polmer_v_pasu))
        polmer_na_R_razdalji.set(np.sqrt(2) * vrednost_polmer_v_pasu)

        prejsnji_polmer_na_R_razdalji.append(np.sqrt(2) * vrednost_polmer_v_pasu)
        prejsnja_rayleighova_razdalja.append(zR)
        prejsnji_divergenci_kot.append(vrednost_valovne_dolzine / (np.pi * vrednost_polmer_v_pasu))
    except ValueError:
        pass

def izracun_za_fokusiranje(*args):
    try:
        vrednost_fokusna_razdalja = float(fokusna_razdalja.get())
        vrednost_pas_leca = float(pas_leca.get())
        vrednost_rayleighova_dolzina_fokusiranje = float(rayleighova_dolzina_fokusiranje.get())
        vrednost_polmer_v_pasu_fokusiranje = float(polmer_v_pasu_fokusiranje.get())

        if vrednost_fokusna_razdalja == vrednost_pas_leca:
            ss = vrednost_fokusna_razdalja
        else:
            ss = -(1 / (vrednost_pas_leca + (vrednost_rayleighova_dolzina_fokusiranje ** 2) /
                        (vrednost_pas_leca - vrednost_fokusna_razdalja)) - 1 / vrednost_fokusna_razdalja) ** (-1)
        leca_pas.set(ss)

        m = (((1-vrednost_pas_leca/vrednost_fokusna_razdalja)**2 +
              (vrednost_rayleighova_dolzina_fokusiranje/vrednost_fokusna_razdalja)**2)**0.5)**(-1)
        polmer_v_pasu_fokusiranje2.set(m * vrednost_polmer_v_pasu_fokusiranje)
        povecava.set(m)
        rayleighova_dolzina_fokusiranje2.set(vrednost_rayleighova_dolzina_fokusiranje * m**2)

    except ValueError:
        pass
# ----------------------------------------------------------------------------------------------------------------------
# Zavihek za snop
zavihek_snop = ttk.Frame(zavihki)
zavihki.add(zavihek_snop, text='Preračun gaussovega snopa')

# -----------------------------------------------------------
# Polje za sliko in tabelo
zgornje_polje_snop = ttk.Frame(zavihek_snop)
zgornje_polje_snop.pack(fill=X)
# Polje za sliko
polje_slika_snop = ttk.Frame(zgornje_polje_snop)
polje_slika_snop.pack(side=LEFT)
# Polje za tabelo
tabela_predhodnih_meritev = ttk.LabelFrame(zgornje_polje_snop, text='Predhodni izračuni:')
tabela_predhodnih_meritev.pack(side=LEFT)


# -----------------------------------------------------------
# Polje za podatke
spodnje_polje_snop = ttk.Frame(zavihek_snop)
spodnje_polje_snop.pack(fill=X)

# Polje za podane podatke
podani_podatki_snop = ttk.Labelframe(spodnje_polje_snop, text='Podani podatki')
podani_podatki_snop.pack(side=LEFT)
# Polja za podatke
ttk.Label(podani_podatki_snop, text='Valovna dolžina:').grid(column=0, row=0, sticky=(W, E))
ttk.Entry(podani_podatki_snop, width=7, textvariable=valovna_dolzina).grid(column=1, row=0, sticky=(W, E))
ttk.Label(podani_podatki_snop, text='nm').grid(column=2, row=0, sticky=(W, E))

ttk.Label(podani_podatki_snop, text='Polmer v pasu snopa:').grid(column=0, row=1, sticky=(W, E))
ttk.Entry(podani_podatki_snop, width=7, textvariable=polmer_v_pasu).grid(column=1, row=1, sticky=(W, E))
ttk.Label(podani_podatki_snop, text='um').grid(column=2, row=1, sticky=(W, E))

ttk.Button(podani_podatki_snop, text='Izračun', command=izracun_za_zarek).grid(column=2, row=3, sticky=(W, E))

# Separator
ttk.Separator(spodnje_polje_snop, orient=VERTICAL).pack(side=LEFT, fill=Y)

# Polje za izračuanane podatke
izracunani_podatki_snop = ttk.Labelframe(spodnje_polje_snop, text='Izračunani podatki')
izracunani_podatki_snop.pack(side=LEFT)

#Polja za podatke
ttk.Label(izracunani_podatki_snop, text='Rayleighova dolžina [zR]:').grid(column=0, row=0, sticky=(W, E))
ttk.Label(izracunani_podatki_snop, textvariable=rayleighova_dolzina).grid(column=1, row=0, sticky=(W, E))
ttk.Label(izracunani_podatki_snop, text='mm').grid(column=2, row=0, sticky=(W, E))
ttk.Label(izracunani_podatki_snop, text='Deivergenčni kot [theta]:').grid(column=0, row=1, sticky=(W, E))
ttk.Label(izracunani_podatki_snop, textvariable=divergencni_kot).grid(column=1, row=1, sticky=(W, E))
ttk.Label(izracunani_podatki_snop, text='mrad').grid(column=2, row=1, sticky=(W, E))
ttk.Label(izracunani_podatki_snop, text='Polmer na Rayleighovi razdalji [w(zR)]:').grid(column=0, row=2, sticky=(W, E))
ttk.Label(izracunani_podatki_snop, textvariable=polmer_na_R_razdalji).grid(column=1, row=2, sticky=(W, E))
ttk.Label(izracunani_podatki_snop, text='um').grid(column=2, row=2, sticky=(W, E))

# ----------------------------------------------------------------------------------------------------------------------
# Zavihek za fokusiranje
zavihek_fokusiranje = ttk.Frame(zavihki)
zavihki.add(zavihek_fokusiranje, text='Preračun fokusiranja')

# -----------------------------------------------------------
# Polje za sliko in tabelo
zgornje_polje_fokusiranje = ttk.Frame(zavihek_fokusiranje)
zgornje_polje_fokusiranje.pack(fill=X)

# -----------------------------------------------------------
# Polje za podatke
spodnje_polje_fokusiranje = ttk.Frame(zavihek_fokusiranje)
spodnje_polje_fokusiranje.pack(fill=X)

# Polje za podane podatke
podani_podatki_fokusiranje = ttk.Labelframe(spodnje_polje_fokusiranje, text='Podani podatki')
podani_podatki_fokusiranje.pack(side=LEFT)
# Polja za podatke
ttk.Label(podani_podatki_fokusiranje, text='Goriščna razdalja [f]:').grid(column=0, row=0, sticky=(W, E))
ttk.Entry(podani_podatki_fokusiranje, width=7, textvariable=fokusna_razdalja).grid(column=1, row=0, sticky=(W, E))
ttk.Label(podani_podatki_fokusiranje, text='mm').grid(column=2, row=0, sticky=(W, E))

ttk.Label(podani_podatki_fokusiranje, text='Razdalja med pasom vstopnega snopa in lečo [s]').grid(column=0, row=1, sticky=(W, E))
ttk.Entry(podani_podatki_fokusiranje, width=7, textvariable=pas_leca).grid(column=1, row=1, sticky=(W, E))
ttk.Label(podani_podatki_fokusiranje, text='mm').grid(column=2, row=1, sticky=(W, E))

ttk.Label(podani_podatki_fokusiranje, text='Rayleighova razdalja vstopnega snopa [zR]:').grid(column=0, row=2, sticky=(W, E))
ttk.Entry(podani_podatki_fokusiranje, width=7, textvariable=rayleighova_dolzina_fokusiranje).grid(column=1, row=2, sticky=(W, E))
ttk.Label(podani_podatki_fokusiranje, text='mm').grid(column=2, row=2, sticky=(W, E))

ttk.Label(podani_podatki_fokusiranje, text='Polmer v pasu vstopnega snopa: [w0]:').grid(column=0, row=3, sticky=(W, E))
ttk.Entry(podani_podatki_fokusiranje, width=7, textvariable=polmer_v_pasu_fokusiranje).grid(column=1, row=3, sticky=(W, E))
ttk.Label(podani_podatki_fokusiranje, text='um').grid(column=2, row=3, sticky=(W, E))

ttk.Button(podani_podatki_fokusiranje, text='Izračun', command=izracun_za_fokusiranje).grid(column=2, row=4, sticky=(W, E))

# Separator
ttk.Separator(spodnje_polje_fokusiranje, orient=VERTICAL).pack(side=LEFT, fill=Y)

# Polje za izračuanane podatke
izracunani_podatki_fokusiranje = ttk.Labelframe(spodnje_polje_fokusiranje, text='Izračunani podatki')
izracunani_podatki_fokusiranje.pack(side=LEFT)

#Polja za podatke
ttk.Label(izracunani_podatki_fokusiranje, text='Razdalja med lečo in pasom izstopnega snopa [s"]:').grid(column=0, row=0, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, textvariable=leca_pas).grid(column=1, row=0, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, text='mm').grid(column=2, row=0, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, text='Polmer izstopnega snopa v pasu [w0"]:').grid(column=0, row=1, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, textvariable=polmer_v_pasu_fokusiranje2).grid(column=1, row=1, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, text='um').grid(column=2, row=1, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, text='Povečava [m]:').grid(column=0, row=2, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, textvariable=povecava).grid(column=1, row=2, sticky=(W, E))
#ttk.Label(izracunani_podatki_fokusiranje, text='mm').grid(column=2, row=2, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, text='Rayleighova razdalja izstopnega snopa [zR"]:').grid(column=0, row=3, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, textvariable=rayleighova_dolzina_fokusiranje2).grid(column=1, row=3, sticky=(W, E))
ttk.Label(izracunani_podatki_fokusiranje, text='mm').grid(column=2, row=3, sticky=(W, E))

# -----------------------------------------------------------
# Separator
ttk.Separator(root, orient=HORIZONTAL).pack(fill=X)
# Credits polje
credits_polje = ttk.Frame(root)
credits_polje.pack(fill=X)
ttk.Label(credits_polje, text='Program napisal Jure Dvoršak - 2019').grid(column=0, row=0, sticky=(W, E))

# ----------------------------------------------------------------------------------------------------------------------

root.mainloop()